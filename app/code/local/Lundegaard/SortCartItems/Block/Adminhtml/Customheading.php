<?php

class Lundegaard_SortCartItems_Block_Adminhtml_CustomHeading extends Mage_Adminhtml_Block_System_Config_Form_Field_Heading {

    /**
     * Render element html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {

        return sprintf('<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5"><div id="%s">%s</div></td></tr>',
            $element->getHtmlId(), $element->getHtmlId(), $this->getHtml($element->getLabel())
        );
    }

    public function getHtml($label = 'Rewrites')
    {
        $rewrites = Mage::getConfig()->getNode()->xpath('//global/blocks/checkout//rewrite//cart');

        $html = ''; //var_export($rewrites, true);

        if(count($rewrites) == 1 && $rewrites[0]->__toString() === 'Lundegaard_SortCartItems_Block_Cart'){
            $rewriteOK = true;
            $style = "color: green;";
        }else{
            $rewriteOK = false;
            $style = "color: red;";
        }

        $html .= "<span style=\"font-weight:bold; font-size: 1.2em;\">$label:</span> ";
        $html .= "<span style=\"font-weight:bold; font-size: 1.2em; $style\">".($rewriteOK ? Mage::helper('sortcartitems')->__('Successful') : Mage::helper('sortcartitems')->__('Failed'))."</span> ";
        $html .= "<span style=\"font-weight:bold; font-size: 1.1em; $style\">";
        if (!$rewriteOK) {
            foreach ($rewrites as $rewrite) {
                if ($rewrite === 'Lundegaard_SortCartItems_Block_Cart') {
                    $html .= "<br/><span style=\"$style\">" . $rewrite."</span>";
                } else {
                    $html .= "<br/>" . $rewrite;
                }
            }
        }
        $html .= "</span>";

        return $html;
    }

}
