<?php
/**
 * Created by PhpStorm.
 * User: jiri.bok
 * Date: 10/7/15
 * Time: 9:01 AM
 */

class Lundegaard_SortCartItems_Block_Cart extends Mage_Checkout_Block_Cart
{
    /**
     * Override the getItems function so it sorts the items
     *
     * @return mixed
     */
    public function getItems()
    {
        $items = parent::getItems();

        // Gets the current store's id
        $storeId = Mage::app()->getStore()->getStoreId();

        $isActive = Mage::getStoreConfig('sortcartitems/settings/active', $storeId);

        if (!$isActive) {
            return $items;
        }

        $sortedProperty = Mage::getStoreConfig('sortcartitems/settings/sortedproperty', $storeId);
        $sortType = Mage::getStoreConfig('sortcartitems/settings/sort', $storeId);

        switch ($sortedProperty) {
            case "productId": $getter = "getProductId"; break;
            case "price":     $getter = "getPrice"; break;
            case "name":
            default:
                $getter = "getName";
        }

        $a = array();

        // Get attributes to sort items by
        if ($getter === "getProductId") {
            foreach ($items as $key => $item) {
                $a[$key] = $item->$getter();
            }
        } else {
            foreach ($items as $key => $item) {
                $a[$key] = $item->getProduct()->$getter();
            }
        }

        array_multisort($a, (int) $sortType, $items);

        return $items;
    }
}