<?php
/**
 * Created by PhpStorm.
 * User: jiri.bok
 * Date: 10/7/15
 * Time: 9:01 AM
 */

class Lundegaard_SortCartItems_Model_Source_SortType {

    /**
     * Get list of sortable properties
     *
     * @param bool $isMultiSelect
     * @return array
     */
    public function toOptionArray($isMultiSelect = false) {

        $options = array(
            array('value' => SORT_ASC,     'label' => Mage::helper('sortcartitems')->__('Ascending order')),
            array('value' => SORT_DESC,    'label' => Mage::helper('sortcartitems')->__('Descending order')),
            array('value' => SORT_REGULAR, 'label' => Mage::helper('sortcartitems')->__('Use standard comparison function')),
            array('value' => SORT_NUMERIC, 'label' => Mage::helper('sortcartitems')->__('Use numeric comparison function')),
            array('value' => SORT_STRING,  'label' => Mage::helper('sortcartitems')->__('Use string comparison function')),
        );

        return $options;
    }

}
