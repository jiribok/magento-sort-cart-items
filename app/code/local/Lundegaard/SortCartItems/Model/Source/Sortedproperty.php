<?php
/**
 * Created by PhpStorm.
 * User: jiri.bok
 * Date: 10/7/15
 * Time: 9:01 AM
 */

class Lundegaard_SortCartItems_Model_Source_SortedProperty {

    /**
     * Get list of sortable properties
     *
     * @param bool $isMultiSelect
     * @return array
     */
    public function toOptionArray($isMultiSelect = false) {

        $options = array(
            array('value' => 'name', 'label' => Mage::helper('sortcartitems')->__('By name')),
            array('value' => 'price', 'label' => Mage::helper('sortcartitems')->__('By price')),
            array('value' => 'productId', 'label' => Mage::helper('sortcartitems')->__('By product id'))
        );

        return $options;
    }

}
